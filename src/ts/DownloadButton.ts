import * as dropdown from './dropdown.js';
import arrayEquals from './arrayEquals.js';
import download from './download.js';

/* Instructions for adding new versions to Merge Box:
 *
 * 1. Add them in assets/handlebars/downloads.json
 * 2. Add `download(...)` lines for every release asset to gulp/taskkinds/fetch_content
 * 3. Add the files below (selections = [version, file_type]) NOTE: The type annotation is quite long; start reading from the equal sign
 */

const download_files: {[key: string]: {
	name: string,
	files: {
		selections: string[],
		file: string
	}[]
}} = {
	mergebox: {
		name: "Merge Box",
		files: [
			{
				selections: ["0.2-1", ".deb"],
				file: "/downloads/mergebox-0.2-1.deb"
			},
			{
				selections: ["0.2-1", "-win32.zip"],
				file: "/downloads/mergebox-0.2-1-win32.zip"
			},
			{
				selections: ["0.2-1", "-win64.zip"],
				file: "/downloads/mergebox-0.2-1-win64.zip"
			},
			{
				selections: ["0.2-1", "-src.tar"],
				file: "/downloads/mergebox-0.2-1-src.tar"
			},
			{
				selections: ["0.2-1", "-src.tar.gz"],
				file: "/downloads/mergebox-0.2-1-src.tar.gz"
			},
			{
				selections: ["0.2-1", "-src.tar.bz2"],
				file: "/downloads/mergebox-0.2-1-src.tar.bz2"
			},
			{
				selections: ["0.2-1", "-src.zip"],
				file: "/downloads/mergebox-0.2-1-src.zip"
			},
			{
				selections: ["0.2", ".deb"],
				file: "/downloads/mergebox-0.2.deb"
			},
			{
				selections: ["0.2", ".apk"],
				file: "/downloads/mergebox-0.2.apk"
			},
			{
				selections: ["0.2", "-win32.zip"],
				file: "/downloads/mergebox-0.2-win32.zip"
			},
			{
				selections: ["0.2", "-win64.zip"],
				file: "/downloads/mergebox-0.2-win64.zip"
			},
			{
				selections: ["0.2", "-src.tar"],
				file: "/downloads/mergebox-0.2-src.tar"
			},
			{
				selections: ["0.2", "-src.tar.gz"],
				file: "/downloads/mergebox-0.2-src.tar.gz"
			},
			{
				selections: ["0.2", "-src.tar.bz2"],
				file: "/downloads/mergebox-0.2-src.tar.bz2"
			},
			{
				selections: ["0.2", "-src.zip"],
				file: "/downloads/mergebox-0.2-src.zip"
			},
			{
				selections: ["0.1", ".deb"],
				file: "/downloads/mergebox-0.1.deb"
			},
			{
				selections: ["0.1", "-win32.zip"],
				file: "/downloads/mergebox-0.1-win32.zip"
			},
			{
				selections: ["0.1", "-win64.zip"],
				file: "/downloads/mergebox-0.1-win64.zip"
			},
			{
				selections: ["0.1", "-src.tar"],
				file: "/downloads/mergebox-0.1-src.tar"
			},
			{
				selections: ["0.1", "-src.tar.gz"],
				file: "/downloads/mergebox-0.1-src.tar.gz"
			},
			{
				selections: ["0.1", "-src.tar.bz2"],
				file: "/downloads/mergebox-0.1-src.tar.bz2"
			},
			{
				selections: ["0.1", "-src.zip"],
				file: "/downloads/mergebox-0.1-src.zip"
			},
		]
	}
};

class DownloadButton extends HTMLElement {
	private rendered: boolean = false;

	private name: string | undefined = undefined;
	private download_button_node: Element | undefined = undefined;
	private selections: Element[] = [];
	public download_url: string|undefined = undefined;
	public download_name: string = "";

	public constructor() {
		super();
	}
	private initSelection(button: Element) {
		var download_button_element = this;
		const div = button.parentElement;
		dropdown.dropdownCreate(button);
		div?.querySelectorAll('div > ul > li').forEach(function(selection: Element) {
			selection.addEventListener('click', function() {
				button.innerHTML = (<HTMLElement>selection).innerText;
				download_button_element.update();
			})
		})
	}
	private render() {
		{ // Find download button node
			const download_button = this.querySelector('div:first-child > button');
			if(download_button != null) {
				this.download_button_node = download_button;
			}
		} { // Adjust download button text and event listener
			if(this.download_button_node == null) {
				console.warn('WEBSITE: DownloadButton does not have real download button');
			} else {
				this.name = this.download_button_node.innerHTML;
				if(download_files[this.name] == undefined) {
					console.warn('WEBSITE: DownloadButton with invalid name: ' + this.name);
				} else {
					this.download_button_node.innerHTML = download_files[this.name].name;
					const download_button = this;
					this.download_button_node.addEventListener('click', function() {
						if(download_button.download_url != undefined) {
							download(download_button.download_url, download_button.download_name);
						}
					})
				}
			}
		} { // Prepare selections
			this.querySelectorAll('div:not(:first-child) > button').forEach(
				element => this.initSelection.call(this, element)
			);
			for(var selection = this.querySelector('div:not(:first-child)'); selection != null; selection = selection.nextElementSibling) {
				if(selection.firstElementChild == null) {
					console.warn('WEBSITE: Invalid element child in HTML of DownloadButton', selection.firstElementChild);
				} else {
					this.selections.push(selection.firstElementChild);
				}
			}
		}
		this.update();
		this.rendered = true;
	}
	public update() {
		if(this.name == undefined) {
			return;
		}
		const selected = this.selections.map(a => a.innerHTML);
		this.download_url = download_files[this.name].files.find(
			a => arrayEquals(selected, a.selections)
		)?.file;
		this.download_name = this.name + "-" + selected.reduce(
			function(previous: string, current: string) {
				return previous + current;
			}
		);
	}
	public connectedCallback() {
		if(!this.rendered) {
			this.render();
		}
	}
};

customElements.define("mergebox-www-site-downloadbutton", DownloadButton);

export default DownloadButton
