function equals_operator<T>(lhs: T, rhs: T) {
	return lhs == rhs;
}

export default function<T>(lhs: T[], rhs: T[], compare: (lhs: T, rhs: T) => boolean = equals_operator) {
	if(lhs.length != rhs.length) {
		return false;
	} else {
		return lhs.map(
			function(_, index) {
				return {
					lhs: lhs[index],
					rhs: rhs[index],
				};
			}
		).map(
			function(item) {
				return item.lhs == item.rhs;
			}
		).reduce(
			function(previous: boolean, current: boolean) {
				return previous && current;
			},
			true
		)
	}
}
