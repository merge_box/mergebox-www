export default function(url: string, dest_name: string | null) {
	var link = document.createElement('a');
	if(dest_name != null) {
		link.download = dest_name;
	} else {
		link.download = "";
	}
	link.href = url;
	link.click();
}
