var openDropdown: Element | null = null;

export function close(): void {
	if(openDropdown != null) {
		openDropdown.classList.remove('site-dropdown-open');
		if(openDropdown.nextElementSibling) {
			(<HTMLElement>openDropdown.nextElementSibling).style.removeProperty('height');
		}
		openDropdown = null;
	}
}

function getViewportHeight(): number {
	var vh = 0
	if(document.documentElement.clientHeight && document.documentElement.clientHeight > vh) {
		vh = document.documentElement.clientHeight
	}
	if(window.innerHeight != undefined && window.innerHeight > vh) {
		vh = window.innerHeight
	}
	return vh;
}

export function open(button: Element): void {
	close();
	openDropdown = button;
	openDropdown.classList.add('site-dropdown-open');

	var menu: Element | null = openDropdown.nextElementSibling;
	if(menu != null) {
		const vh = getViewportHeight();
		const preferredHeight = (<HTMLElement>menu).scrollHeight;
		if(0.3 * vh < preferredHeight) {
			(<HTMLElement>menu).style.height = (0.3*vh).toString() + "px";
		} else {
			(<HTMLElement>menu).style.height = preferredHeight.toString() + "px";
		}
	}
}

export function toggle(button: Element): void {
	if(openDropdown == button) {
		close();
	} else {
		open(button);
	}
}

document.addEventListener('click', function(event) {
	const target: EventTarget | null = event.target;
	
	var checkElement: Element | null = <Element>target;
	var foundElement: Element | null = null;
	while(checkElement) {
		if(checkElement.classList.contains('site-dropdown')) {
			foundElement = checkElement;
			break;
		}
		if(
			checkElement.previousElementSibling
				?  checkElement.previousElementSibling.classList.contains('site-dropdown')
				: false
		) {
			foundElement = checkElement.previousElementSibling;
			break;
		}
		checkElement = checkElement.parentElement;
	}
	if(foundElement) {
		toggle(foundElement);
	} else {
		close();
	}
})

export function dropdownCreate(button: Element) {
	button.classList.add('site-dropdown');
}
