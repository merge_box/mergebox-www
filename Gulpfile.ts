import * as gulp from 'gulp';
import tasks from './gulp';
import Task from './gulp/Task';

const clean = tasks.clean();

const incremental_build: Task = gulp.parallel(
	tasks.copyDownloads(),
	tasks.handlebars(),
	tasks.png(),
	tasks.svg(),
	tasks.sass(true),
	tasks.typescript(true),
);

const fetchContent: Task = tasks.fetchContent(false);

const build: Task = gulp.series(
	clean,
	tasks.fetchContent(true),
	incremental_build,
);

const serve: Task = tasks.serve(tasks.noop());

const test: Task = gulp.series(
	clean,
	tasks.fetchContent(true),
	tasks.serve(incremental_build),
)

export = {
	fetch: fetchContent,
	clean,
	build,
	test,
	serve,
	default: build
}
