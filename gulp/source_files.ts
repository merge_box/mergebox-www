import Constants from './constants';
import pattern from './pattern';

function withSuffix(suffix: string): string {
	return pattern.join(
		pattern.allChildren(Constants.SOURCE_DIRECTORY),
		pattern.suffixedFiles(suffix),
	)
}

export default {
	withSuffix,
}
