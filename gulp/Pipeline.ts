import StepGenerator from './StepGenerator';
import WrapperStream from './WrapperStream';

class Pipeline {
	protected steps: StepGenerator[];
	public constructor(steps?:StepGenerator[]) {
		this.steps = steps? steps : [];
	}
	public addStep(step: StepGenerator) {
		this.steps.push(step);
	}
	public wrap(wrapper: WrapperStream) {
		this.steps = wrapper.initiator.steps.concat(this.steps, wrapper.terminator.steps);
		return this
	}
	public assemble(): NodeJS.ReadWriteStream {
		return this.steps.map((i) => i.generate()).reduce((previous, current) => {
			return previous.pipe(current);
		});
	}
};
export default Pipeline
