import clean from './taskkinds/clean';
import handlebars from './taskkinds/handlebars';
import noop from './taskkinds/noop';
import png from './taskkinds/png';
import svg from './taskkinds/svg';
import sass from './taskkinds/sass';
import serve from './taskkinds/serve';
import typescript from './taskkinds/typescript';
import fetchContent from './taskkinds/fetch_content';
import copyDownloads from './taskkinds/copy_downloads';

export default {
	clean,
	fetchContent,
	copyDownloads,
	handlebars,
	png,
	svg,
	sass,
	typescript,
	serve,
	noop,
}
