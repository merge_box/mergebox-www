type Task = ((cb: any) => void) | (() => NodeJS.ReadWriteStream);

export default Task;
