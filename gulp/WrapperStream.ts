import Pipeline from './Pipeline';

class WrapperStream {
	public initiator: Pipeline;
	public terminator: Pipeline;
};
export default WrapperStream;
