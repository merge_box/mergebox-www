import StepGenerator from './StepGenerator';

class PluginStepGenerator implements StepGenerator {
	private func: any;
	private args: any[];
	public constructor(func: CallableFunction, ...args: any[]) {
		this.func = func;
		this.args = args;
	}
	public generate() {
		return this.func(...this.args);
	}
};
export default PluginStepGenerator;
