import * as fs from 'fs';
import * as path from 'path';
import * as gulp from 'gulp';

import Task from '../Task';
import Pipeline from '../Pipeline';
import PluginStepGenerator from '../PluginStepGenerator';
import Constants from '../constants';

const gulpdownloadfiles = require('gulp-download-files') as any;
import * as gulprename from 'gulp-rename';

function download(incremental: boolean, dest: string|null, url: string): Task {
	return function(cb) {
		if(dest != null) {
			if(incremental && fs.existsSync(path.join(Constants.FETCH_DIRECTORY, dest))) {
				cb();
				return;
			}
		}
		// Otherwise download
		var steps = [new PluginStepGenerator(gulpdownloadfiles, url)];
		if(dest != null) {
			steps.push(new PluginStepGenerator(gulprename, dest))
		}
		steps.push(new PluginStepGenerator(gulp.dest, Constants.FETCH_DIRECTORY));
		return new Pipeline(steps).assemble();
	}
}

export default (incremental: boolean): Task => gulp.series(
	download(incremental, 'downloads/mergebox-0.2-1.deb', 'https://gitlab.com/merge_box/mergebox/uploads/27ff08a9ac98038f97365787e0aec861/merge_box-0.2-1.deb'),
	download(incremental, 'downloads/mergebox-0.2-1-win32.zip', 'https://gitlab.com/merge_box/mergebox/uploads/1728f63d57b18344995d66e9483ccdab/merge_box-0.2-1_32.zip'),
	download(incremental, 'downloads/mergebox-0.2-1-win64.zip', 'https://gitlab.com/merge_box/mergebox/uploads/f08066d8f305c67c69cc6214347c3018/merge_box-0.2-1_64.zip'),
	download(incremental, 'downloads/mergebpx-0.2-1-src.tar', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2-1/mergebox-0.2-1.tar'),
	download(incremental, 'downloads/mergebpx-0.2-1-src.tar.gz', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2-1/mergebox-0.2-1.tar.gz'),
	download(incremental, 'downloads/mergebpx-0.2-1-src.tar.bz2', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2-1/mergebox-0.2-1.tar.bz2'),
	download(incremental, 'downloads/mergebpx-0.2-1-src.zip', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2-1/mergebox-0.2-1.zip'),
	download(incremental, 'downloads/mergebox-0.2.deb', 'https://gitlab.com/merge_box/mergebox/uploads/f046862d9e079b9e448e2a721e3deb30/merge_box.deb'),
	download(incremental, 'downloads/mergebox-0.2.apk', 'https://gitlab.com/merge_box/mergebox/uploads/84ebeaac362d55eec33267230cf96a1a/merge_box.apk'),
	download(incremental, 'downloads/mergebox-0.2-win32.zip', 'https://gitlab.com/merge_box/mergebox/uploads/84ebeaac362d55eec33267230cf96a1a/merge_box.apk'),
	download(incremental, 'downloads/mergebox-0.2-win64.zip', 'https://gitlab.com/merge_box/mergebox/uploads/9e8bc190a27c49f62a7c8518411a9b39/merge_box-win64.zip'),
	download(incremental, 'downloads/mergebox-0.2-src.tar', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2/mergebox-0.2.tar'),
	download(incremental, 'downloads/mergebox-0.2-src.tar.gz', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2/mergebox-0.2.tar.gz'),
	download(incremental, 'downloads/mergebox-0.2-src.tar.bz2', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2/mergebox-0.2.tar.bz2'),
	download(incremental, 'downloads/mergebox-0.2-src.zip', 'https://gitlab.com/merge_box/mergebox/-/archive/0.2/mergebox-0.2.zip'),
	download(incremental, 'downloads/mergebox-0.1.deb', 'https://gitlab.com/debiankaios/merge_box/uploads/1e63d70b2efa635c4ad88057808f5a36/mergebox-0.1-2.deb'),
	download(incremental, 'downloads/mergebox-0.1-win32.zip', 'https://gitlab.com/debiankaios/merge_box/uploads/3eaab487ebcc8763b35233fc776f20fd/exe-32.zip'),
	download(incremental, 'downloads/mergebox-0.1-win64.zip', 'https://gitlab.com/debiankaios/merge_box/uploads/5302eae5c182d916932219ae7ea57bd6/exe-64.zip'),
	download(incremental, 'downloads/mergebox-0.1-src.tar', 'https://gitlab.com/debiankaios/merge_box/-/archive/0.1/merge_box-0.1.tar'),
	download(incremental, 'downloads/mergebox-0.1-src.tar.gz', 'https://gitlab.com/debiankaios/merge_box/-/archive/0.1/merge_box-0.1.tar.gz'),
	download(incremental, 'downloads/mergebox-0.1-src.tar.bz2', 'https://gitlab.com/debiankaios/merge_box/-/archive/0.1/merge_box-0.1.tar.bz2'),
	download(incremental, 'downloads/mergebox-0.1-src.zip', 'https://gitlab.com/debiankaios/merge_box/-/archive/0.1/merge_box-0.1.zip'),
)
