import * as gulp from 'gulp';
const gulpliveserver = require('gulp-live-server') as any;

import Constants from '../constants';
import pattern from '../pattern';
import Task from '../Task';

export default (rebuild_task: gulp.TaskFunction): Task => gulp.series(
	rebuild_task,
	() => {
		var server = gulpliveserver.static(Constants.BUILD_DIRECTORY);
		server.start();
		gulp.watch(
			[
				pattern.allChildren(Constants.SOURCE_DIRECTORY),
				pattern.allChildren(Constants.ASSETS_DIRECTORY),
			],
			rebuild_task
		);
		gulp.watch(
			pattern.allChildren(Constants.BUILD_DIRECTORY),
			function(file) {
				server.notify.apply(server, [file]);
			}
		);
	}
)
