import * as gulptypescript from 'gulp-typescript';
import * as gulpuglify from 'gulp-uglify';

import constants from '../constants';
import generatePipelineTask from '../generatePipelineTask';
import IOWrapper from '../IOWrapper';
import Pipeline from '../Pipeline';
import PluginStepGenerator from '../PluginStepGenerator';
import SourcemapWrapper from '../SourcemapWrapper';
import source_files from '../source_files';
import Task from '../Task';

export default (sourcemaps_dest: string | boolean = false): Task => generatePipelineTask(
	new Pipeline([
		new PluginStepGenerator(gulptypescript, {
			baseUrl: constants.SOURCE_DIRECTORY,
			esModuleInterop: true,
			module: 'es6',
			target: 'es6',
			moduleResolution: 'node',
			noImplicitAny: true,
			noImplicitOverride: true,
			removeComments: true,
			strict: true,
			strictNullChecks: true,
			strictPropertyInitialization: true,
			useUnknownInCatchVariables: true,
		}),
		new PluginStepGenerator(gulpuglify, {}),
	]).wrap(
		SourcemapWrapper(sourcemaps_dest)
	).wrap(
		IOWrapper(source_files.withSuffix(".ts"))
	)
)
