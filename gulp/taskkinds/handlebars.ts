const gulphb = require('gulp-hb') as any;
import * as gulphtmlmin from 'gulp-htmlmin';

import generatePipelineTask from '../generatePipelineTask';
import IOWrapper from '../IOWrapper';
import Pipeline from '../Pipeline';
import Task from '../Task';

import PluginStepGenerator from '../PluginStepGenerator';
import source_files from '../source_files';

export default (): Task => generatePipelineTask(
	new Pipeline([
		new PluginStepGenerator(
			() => gulphb()
				.partials('assets/handlebars/*.hbs')
				.helpers('assets/handlebars/*.js')
				.data('assets/handlebars/*.json')
		),
		new PluginStepGenerator(gulphtmlmin, {
			collapseInlineTagWhitespace: true,
			collapseWhitespace: true,
			decodeEntities: true,
			minifyCSS: true,
			minifyJS: true,
			removeComments: true,
			removeRedundantAttributes: true,
			removeScriptTypeAttributes: true,
			removeStyleLinkTypeAttributes: true,
			sortAttributes: true,
			sortClassName: true,
		}),
	]).wrap(IOWrapper(source_files.withSuffix(".html")))
)
