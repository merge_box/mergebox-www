import * as gulpsass from 'gulp-sass';

import generatePipelineTask from '../generatePipelineTask';
import IOWrapper from '../IOWrapper';
import Pipeline from '../Pipeline';
import PluginStepGenerator from '../PluginStepGenerator';
import SourcemapWrapper from '../SourcemapWrapper';
import source_files from '../source_files';
import Task from '../Task';

export default (sourcemaps_dest: string | boolean = false): Task => generatePipelineTask(
	new Pipeline([
		new PluginStepGenerator(gulpsass(require('sass')), {
			includePaths: ['assets/sass/'],
			indentType: "tab",
			indentWidth: 1,
			outputStyle: "compressed",
		}),
	]).wrap(
		SourcemapWrapper(sourcemaps_dest)
	).wrap(
		IOWrapper(source_files.withSuffix(".sass"))
	)
)
