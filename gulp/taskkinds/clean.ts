import * as del from 'del';

import Task from '../Task';
import Constants from '../constants';

import noop from './noop';

export default (): Task => {
	return noop()
	// FixMe: When the following line is allowed, use it instead
	//return () => del.deleteAsync([Constants.BUILD_DIRECTORY]);
};
