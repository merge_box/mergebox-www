import generatePipelineTask from '../generatePipelineTask';
import IOWrapper from '../IOWrapper';
import Pipeline from '../Pipeline';
import source_files from '../source_files';
import Task from '../Task';
import gulpsvgmin = require('gulp-svgmin');
import PluginStepGenerator from '../PluginStepGenerator';

export default (): Task => generatePipelineTask(
	new Pipeline([
		new PluginStepGenerator(gulpsvgmin)
	]).wrap(IOWrapper(source_files.withSuffix(".svg")))
)
