import generatePipelineTask from '../generatePipelineTask';
import IOWrapper from '../IOWrapper';
import Pipeline from '../Pipeline';
import source_files from '../source_files';
import Task from '../Task';

export default (): Task => generatePipelineTask(
	new Pipeline([]).wrap(IOWrapper(source_files.withSuffix(".png")))
)
