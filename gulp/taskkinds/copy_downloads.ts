import generatePipelineTask from '../generatePipelineTask';
import IOWrapper from '../IOWrapper';
import Pipeline from '../Pipeline';
import pattern from '../pattern';
import Task from '../Task';
import Constants from '../constants';

import * as gulprename from 'gulp-rename';
import PluginStepGenerator from '../PluginStepGenerator';

export default (): Task => generatePipelineTask(
	new Pipeline([
		new PluginStepGenerator(gulprename, {
			dirname: 'downloads',
		})
	]).wrap(IOWrapper(
		pattern.allChildren(pattern.join(Constants.FETCH_DIRECTORY, 'downloads'))
	))
)
