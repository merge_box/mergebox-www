import * as gulp from 'gulp';

import Constants from './constants';
import Pipeline from './Pipeline';
import PluginStepGenerator from './PluginStepGenerator';
import WrapperStream from './WrapperStream';

export default (inputPattern: string|string[]): WrapperStream => ({
	initiator: new Pipeline([new PluginStepGenerator(gulp.src, inputPattern)]),
	terminator: new Pipeline([new PluginStepGenerator(gulp.dest, Constants.BUILD_DIRECTORY)])
})
