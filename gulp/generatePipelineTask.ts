import Pipeline from './Pipeline';
import Task from './Task';

export default function (pipeline: Pipeline): Task {
	return () => pipeline.assemble();
}
