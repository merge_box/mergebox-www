interface StepGenerator {
	generate: () => NodeJS.ReadWriteStream,
};
export default StepGenerator
