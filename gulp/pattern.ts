const CWD: string = ".";
const SEPARATOR: string = "/";

function join(...patterns: string[]): string {
	return patterns.reduce(function(prev_value, current_value) {
		if(prev_value.endsWith(SEPARATOR)) {
			return prev_value + current_value
		} else {
			return prev_value + SEPARATOR + current_value
		}
	})
}

function allChildren(parent: string): string {
	return join(parent, "**")
}

function suffixedFiles(suffix: string): string {
	return "*" + suffix;
}

export default {
	CWD,
	SEPARATOR,
	join,
	allChildren,
	suffixedFiles,
}
