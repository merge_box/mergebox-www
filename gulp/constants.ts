export default {
	SOURCE_DIRECTORY: 'src',
	ASSETS_DIRECTORY: 'assets',
	FETCH_DIRECTORY: 'fetch',
	BUILD_DIRECTORY: 'build',
}
