import * as gulpsourcemaps from 'gulp-sourcemaps';

import Pipeline from './Pipeline';
import PluginStepGenerator from './PluginStepGenerator';
import WrapperStream from './WrapperStream';

export default (destination: string|boolean = false): WrapperStream => destination? {
	initiator: new Pipeline([new PluginStepGenerator(gulpsourcemaps.init)]),
	terminator: new Pipeline([new PluginStepGenerator(gulpsourcemaps.write, typeof destination == 'string'? destination : undefined)])
} : {
	initiator: new Pipeline([]),
	terminator: new Pipeline([]),
};
