# mergebox-www

This is the repository for the website of [merge box](https://gitlab.com/merge_box/mergebox).

## Setup

For preparation, the dependencies (including developer ones) need to be installed:

~~~sh
npm install
~~~

Then, for testing:
~~~sh
npm run test
~~~
The above command will output a URL at which you can look on the site and watch out for file changes.

When the site should get moved to a server:
~~~sh
npm run build
~~~
will generate the `build/` directory containing the files to put into the html root directory.
